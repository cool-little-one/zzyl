package com.zzyl.nursing.service;

import java.util.List;

import com.aliyun.iot20180120.models.DeleteDeviceRequest;
import com.aliyun.iot20180120.models.QueryDevicePropertyStatusRequest;
import com.zzyl.common.core.domain.AjaxResult;
import com.zzyl.nursing.domain.Device;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.nursing.dto.DeviceDto;
import com.zzyl.nursing.vo.DeviceVo;
import com.zzyl.nursing.vo.ProductVo;

/**
 * 设备Service接口
 * 
 * @author ruoyi
 * @date 2024-10-14
 */
public interface IDeviceService extends IService<Device>
{
    /**
     * 查询设备
     * 
     * @param id 设备主键
     * @return 设备
     */
    public Device selectDeviceById(Long id);

    /**
     * 查询设备列表
     * 
     * @param device 设备
     * @return 设备集合
     */
    public List<Device> selectDeviceList(Device device);

    /**
     * 新增设备
     * 
     * @param device 设备
     * @return 结果
     */
    public int insertDevice(Device device);

    /**
     * 修改设备
     * 
     * @param device 设备
     * @return 结果
     */
    public int updateDevice(Device device);

    /**
     * 批量删除设备
     * 
     * @param ids 需要删除的设备主键集合
     * @return 结果
     */
    public int deleteDeviceByIds(Long[] ids);

    /**
     * 删除设备信息
     * 
     * @param id 设备主键
     * @return 结果
     */
    public int deleteDeviceById(Long id);

    /**
     * 从阿里云同步设备列表
     */
    void syncProductList();

    /**
     * 从Redis中获取所有产品列表
     * @return  产品列表
     */
    List<ProductVo> allProduct();

    /**
     * 注册设备
     * @param deviceDto 设备数据
     */
    void register(DeviceDto deviceDto);

    /**
     * 查询设备详情
     * @param deviceDto 设备信息
     * @return  设备详情
     */
    DeviceVo queryDeviceDetail(DeviceDto deviceDto);

    /**
     * 查看指定产品的已发布的物模型的功能定义详情
     * @param deviceDto 设备信息
     * @return  物模型数据
     */
    AjaxResult queryThingModelPublished(DeviceDto deviceDto);

    /**
     * 查询物模型运行状态
     * @param request   请求参数
     * @return 结果
     */
    AjaxResult queryDevicePropertyStatus(QueryDevicePropertyStatusRequest request);

    /**
     * 修改设备昵称
     * @param device    设备
     */
    void updateDeviceNickName(Device device);

    /**
     * 删除设备信息
     * @return 结果
     */
    int deleteDevice(DeleteDeviceRequest request);
}
