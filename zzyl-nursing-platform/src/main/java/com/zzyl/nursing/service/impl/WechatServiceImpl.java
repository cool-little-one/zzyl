package com.zzyl.nursing.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.zzyl.nursing.service.WechatService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class WechatServiceImpl implements WechatService {

    // 登录
    private static final String REQUEST_URL = "https://api.weixin.qq.com/sns/jscode2session?grant_type=authorization_code";

    // 获取token
    private static final String TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";

    // 获取手机号
    private static final String PHONE_REQUEST_URL = "https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=";

    @Value("${wechat.appid}")
    private String appid;

    @Value("${wechat.appsecret}")
    private String secret;

    /**
     * 获取openid
     *
     * @param code 小程序端传的code
     * @return openid
     */
    @Override
    public String getOpenid(String code) {

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("appid", appid);
        paramMap.put("secret", secret);
        paramMap.put("js_code", code);

        String result = HttpUtil.get(REQUEST_URL, paramMap);

        // 返回的JSON格式的数据，将结果转成JSON对象
        JSONObject jsonObject = JSONUtil.parseObj(result);

        // 先判断失败
        if (ObjectUtil.isNotEmpty(jsonObject.getInt("errcode"))) {
            throw new RuntimeException(jsonObject.getStr("errmsg"));
        }

        // 成功了，返回openid

        return jsonObject.getStr("openid");
    }

    /**
     * 获取手机号
     *
     * @param detailCode 小程序端传的detailCode
     * @return 手机号
     */
    @Override
    public String getPhoneNumber(String detailCode) {
        String token = getToken();

        // 封装请求参数
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("code", detailCode);

        String result = HttpUtil.post(PHONE_REQUEST_URL + token, JSONUtil.toJsonStr(paramMap));

        // 返回的JSON格式的数据，将结果转成JSON对象
        JSONObject jsonObject = JSONUtil.parseObj(result);

        // 先判断异常情况
        if (!jsonObject.getInt("errcode").equals(0)) {
            throw new RuntimeException(jsonObject.getStr("errmsg"));
        }

        return jsonObject.getJSONObject("phone_info").getStr("phoneNumber");
    }

    /**
     * 获取一个token
     * @return
     */
    private String getToken() {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("appid", appid);
        paramMap.put("secret", secret);

        String result = HttpUtil.get(TOKEN_URL, paramMap);
        JSONObject jsonObject = JSONUtil.parseObj(result);
        // 先判断失败
        if (ObjectUtil.isNotEmpty(jsonObject.getInt("errcode"))) {
            throw new RuntimeException(jsonObject.getStr("errmsg"));
        }

        return jsonObject.getStr("access_token");
    }
}
