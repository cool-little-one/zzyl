package com.zzyl.nursing.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzyl.common.core.page.TableDataInfo;
import com.zzyl.common.utils.DateUtils;
import com.zzyl.common.utils.StringUtils;
import com.zzyl.nursing.dto.ElderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zzyl.nursing.mapper.ElderMapper;
import com.zzyl.nursing.domain.Elder;
import com.zzyl.nursing.service.IElderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.Arrays;

/**
 * 老人Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-10-07
 */
@Service
public class ElderServiceImpl extends ServiceImpl<ElderMapper, Elder> implements IElderService
{
    @Autowired
    private ElderMapper elderMapper;

    /**
     * 查询老人
     * 
     * @param id 老人主键
     * @return 老人
     */
    @Override
    public Elder selectElderById(Long id)
    {
        return getById(id);
    }

    /**
     * 查询老人列表
     * 
     * @param elder 老人
     * @return 老人
     */
    @Override
    public List<Elder> selectElderList(Elder elder)
    {
        return elderMapper.selectElderList(elder);
    }

    /**
     * 新增老人
     * 
     * @param elder 老人
     * @return 结果
     */
    @Override
    public int insertElder(Elder elder)
    {
        return save(elder) ? 1 : 0;
    }

    /**
     * 修改老人
     * 
     * @param elder 老人
     * @return 结果
     */
    @Override
    public int updateElder(Elder elder)
    {
        return updateById(elder) ? 1 : 0;
    }

    /**
     * 批量删除老人
     * 
     * @param ids 需要删除的老人主键
     * @return 结果
     */
    @Override
    public int deleteElderByIds(Long[] ids)
    {
        return removeByIds(Arrays.asList(ids)) ? 1 : 0;
    }

    /**
     * 删除老人信息
     * 
     * @param id 老人主键
     * @return 结果
     */
    @Override
    public int deleteElderById(Long id)
    {
        return removeById(id) ? 1 : 0;
    }

    /**
     * 条件分页查询老人列表
     *
     * @param elderDto 分页条件
     * @return 老人列表
     */
    @Override
    public TableDataInfo<Elder> pageQuery(ElderDto elderDto) {

        Page<Elder> page = new Page<>(elderDto.getPageNum(), elderDto.getPageSize());

        LambdaQueryWrapper<Elder> queryWrapper = new LambdaQueryWrapper<>();

        // 构建查询条件
        queryWrapper.eq(Elder::getStatus, 1)
                .like(StringUtils.isNotEmpty(elderDto.getName()), Elder::getName, elderDto.getName())
                .like(StringUtils.isNotEmpty(elderDto.getIdCardNo()), Elder::getIdCardNo, elderDto.getIdCardNo());

        page = page(page, queryWrapper);

        return getTableDataInfo(page);
    }

    private TableDataInfo<Elder> getTableDataInfo(Page<Elder> page) {
        TableDataInfo<Elder> tableDataInfo = new TableDataInfo<>();
        tableDataInfo.setCode(200);
        tableDataInfo.setMsg("查询成功");
        tableDataInfo.setTotal(page.getTotal());
        tableDataInfo.setRows(page.getRecords());

        return tableDataInfo;
    }
}
