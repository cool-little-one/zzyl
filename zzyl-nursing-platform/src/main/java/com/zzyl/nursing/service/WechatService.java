package com.zzyl.nursing.service;

public interface WechatService {
    /**
     * 获取openid
     * @param code  小程序端传的code
     * @return  openid
     */
    String getOpenid(String code);

    /**
     * 获取手机号
     * @param detailCode    小程序端传的detailCode
     * @return  手机号
     */
    String getPhoneNumber(String detailCode);
}
