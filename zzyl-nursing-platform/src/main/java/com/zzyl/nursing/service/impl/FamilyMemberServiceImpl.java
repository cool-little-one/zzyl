package com.zzyl.nursing.service.impl;

import java.util.*;

import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.zzyl.common.exception.base.BaseException;
import com.zzyl.common.utils.DateUtils;
import com.zzyl.common.utils.StringUtils;
import com.zzyl.framework.web.service.TokenService;
import com.zzyl.nursing.dto.UserLoginRequestDto;
import com.zzyl.nursing.service.WechatService;
import com.zzyl.nursing.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zzyl.nursing.mapper.FamilyMemberMapper;
import com.zzyl.nursing.domain.FamilyMember;
import com.zzyl.nursing.service.IFamilyMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 老人家属Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-10-13
 */
@Service
public class FamilyMemberServiceImpl extends ServiceImpl<FamilyMemberMapper, FamilyMember> implements IFamilyMemberService
{
    @Autowired
    private FamilyMemberMapper familyMemberMapper;

    @Autowired
    private WechatService wechatService;

    @Autowired
    private TokenService tokenService;

    static List<String> DEFAULT_NICKNAME_PREFIX = ListUtil.of("生活更美好",
            "大桔大利",
            "日富一日",
            "好柿开花",
            "柿柿如意",
            "一椰暴富",
            "大柚所为",
            "杨梅吐气",
            "天生荔枝"
    );

    /**
     * 查询老人家属
     * 
     * @param id 老人家属主键
     * @return 老人家属
     */
    @Override
    public FamilyMember selectFamilyMemberById(Long id)
    {
        return getById(id);
    }

    /**
     * 查询老人家属列表
     * 
     * @param familyMember 老人家属
     * @return 老人家属
     */
    @Override
    public List<FamilyMember> selectFamilyMemberList(FamilyMember familyMember)
    {
        return familyMemberMapper.selectFamilyMemberList(familyMember);
    }

    /**
     * 新增老人家属
     * 
     * @param familyMember 老人家属
     * @return 结果
     */
    @Override
    public int insertFamilyMember(FamilyMember familyMember)
    {
        return save(familyMember) ? 1 : 0;
    }

    /**
     * 修改老人家属
     * 
     * @param familyMember 老人家属
     * @return 结果
     */
    @Override
    public int updateFamilyMember(FamilyMember familyMember)
    {
        return updateById(familyMember) ? 1 : 0;
    }

    /**
     * 批量删除老人家属
     * 
     * @param ids 需要删除的老人家属主键
     * @return 结果
     */
    @Override
    public int deleteFamilyMemberByIds(Long[] ids)
    {
        return removeByIds(Arrays.asList(ids)) ? 1 : 0;
    }

    /**
     * 删除老人家属信息
     * 
     * @param id 老人家属主键
     * @return 结果
     */
    @Override
    public int deleteFamilyMemberById(Long id)
    {
        return removeById(id) ? 1 : 0;
    }

    /**
     * 小程序登录
     *
     * @param userLoginRequestDto 小程序传递的参数
     * @return token和nickName
     */
    @Override
    public LoginVo login(UserLoginRequestDto userLoginRequestDto) {
        // 1.获取openid
        String openid = wechatService.getOpenid(userLoginRequestDto.getCode());
        if (StringUtils.isEmpty(openid)) {
            throw new BaseException("获取openid失败");
        }
        // 2.根据openid查询数据库，判断是不是新用户
        FamilyMember familyMember = familyMemberMapper
                .selectOne(Wrappers.<FamilyMember>lambdaQuery().eq(FamilyMember::getOpenId, openid));

        // 3.如果上一步返回的结果为null，证明是新用户，构建用户对象，并赋予openid
        if (ObjectUtils.isEmpty(familyMember)) {
            // 新建一个用户
//            familyMember = new FamilyMember();
//            familyMember.setOpenId(openid);
            // 使用构建者模式简化构建对象的逻辑
            familyMember = FamilyMember.builder().openId(openid).build();
        }

        // 4.调用微信接口获取手机号
        String phoneNumber = wechatService.getPhoneNumber(userLoginRequestDto.getPhoneCode());

        if (StringUtils.isEmpty(phoneNumber)) {
            throw new BaseException("获取手机号失败");
        }

        // 5.调用方法新增或者修改用户，如果是新增用户，还要给用户生成一个昵称
        saveOrUpdateFamilyMember(familyMember, phoneNumber);

        // 6.把用户id和昵称封装到jwt返回
        Map<String, Object> claims = new HashMap<>();
        claims.put("userId", familyMember.getId());
        claims.put("nickName", familyMember.getName());
        String token = tokenService.createToken(claims);

        LoginVo loginVo = new LoginVo();
        loginVo.setToken(token);
        loginVo.setNickName(familyMember.getName());
        return loginVo;
    }

    /**
     * 新增或者修改用户
     * @param familyMember  用户信息
     * @param phoneNumber   手机号
     */
    private void saveOrUpdateFamilyMember(FamilyMember familyMember, String phoneNumber) {
        // 1.判断用户对象中的手机号和本次获取的手机号是否不同
        if (!phoneNumber.equals(familyMember.getPhone())) {
            // 手机号要更新
            familyMember.setPhone(phoneNumber);
        }
        // 2.判断是新增用户还是更新用户
        if (ObjectUtils.isNotEmpty(familyMember.getId())) {
            // 如果id不为空，更新用户
            updateById(familyMember);
            return;
        }
        // 3.新增用户
        // 生成昵称
        int index = (int) (Math.random() * DEFAULT_NICKNAME_PREFIX.size());
        String result = DEFAULT_NICKNAME_PREFIX.get(index);
        familyMember.setName(result + phoneNumber.substring(7));
        save(familyMember);
    }
}
