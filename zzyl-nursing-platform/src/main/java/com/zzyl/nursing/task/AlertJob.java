package com.zzyl.nursing.task;

import com.zzyl.nursing.service.IAlertRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AlertJob {
    @Autowired
    private IAlertRuleService alertRuleService;

    public void deviceDataAlertFilter() {
        alertRuleService.alertFilter();
    }
}
