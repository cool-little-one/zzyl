package com.zzyl.nursing.task;

import com.zzyl.nursing.service.IContractService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Slf4j
@Component
public class UpdateContractStatusJob {

    @Autowired
    private IContractService contractService;

    public void updateContractStatus() {
        log.info("开始更新合同状态：{}", LocalDateTime.now());
        contractService.updateContractStatus();
    }
}
