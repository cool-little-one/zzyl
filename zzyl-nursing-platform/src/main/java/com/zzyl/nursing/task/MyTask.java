package com.zzyl.nursing.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Slf4j
public class MyTask {
    // 从第0秒开始，每隔5秒执行一次
//    @Scheduled(cron = "0/5 * * * * ?")
    public void run() {
        log.info("定时任务执行了：{}", LocalDateTime.now());
    }
}
