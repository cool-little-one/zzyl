package com.zzyl.nursing.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 老人信息
 *
 * @author itcast
 * @create 2023/12/18 20:11
 **/
@ApiModel(description = "老人分页信息")
@Data
public class ElderDto {
    /**
     * 老人姓名
     */
    @ApiModelProperty(value = "当前页")
    private Integer pageNum;


    /**
     * 身份证号
     */
    @ApiModelProperty(value = "每页条数")
    private Integer pageSize;

    /**
     * 老人姓名
     */
    @ApiModelProperty(value = "老人姓名")
    private String name;


    /**
     * 身份证号
     */
    @ApiModelProperty(value = "身份证号")
    private String idCardNo;

    /**
     * 亲属关系
     */
    @ApiModelProperty(value = "状态（0：禁用，1：启用）")
    private Integer status;

}