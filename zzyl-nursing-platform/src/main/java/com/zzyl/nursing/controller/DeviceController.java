package com.zzyl.nursing.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.aliyun.iot20180120.models.DeleteDeviceRequest;
import com.aliyun.iot20180120.models.QueryDevicePropertyStatusRequest;
import com.zzyl.common.core.domain.R;
import com.zzyl.nursing.dto.DeviceDto;
import com.zzyl.nursing.vo.DeviceVo;
import com.zzyl.nursing.vo.ProductVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.zzyl.common.annotation.Log;
import com.zzyl.common.core.controller.BaseController;
import com.zzyl.common.core.domain.AjaxResult;
import com.zzyl.common.enums.BusinessType;
import com.zzyl.nursing.domain.Device;
import com.zzyl.nursing.service.IDeviceService;
import com.zzyl.common.utils.poi.ExcelUtil;
import com.zzyl.common.core.page.TableDataInfo;

/**
 * 设备Controller
 * 
 * @author ruoyi
 * @date 2024-10-14
 */
@RestController
@RequestMapping("/nursing/device")
@Api(tags = "设备相关接口")
public class DeviceController extends BaseController
{
    @Autowired
    private IDeviceService deviceService;

    /**
     * 查询设备列表
     */
    @PreAuthorize("@ss.hasPermi('nursing:device:list')")
    @GetMapping("/list")
    @ApiOperation(value = "获取设备列表")
    public TableDataInfo<List<Device>> list(Device device)
    {
        startPage();
        List<Device> list = deviceService.selectDeviceList(device);
        return getDataTable(list);
    }

    @PostMapping("/syncProductList")
    public AjaxResult syncProductList() {
        deviceService.syncProductList();
        return success();
    }

    @GetMapping("/allProduct")
    public R<List<ProductVo>> allProduct() {
        List<ProductVo> productList = deviceService.allProduct();

        return R.ok(productList);
    }

    @PostMapping("/register")
    public AjaxResult register(@RequestBody DeviceDto deviceDto) {
        deviceService.register(deviceDto);
        return success();
    }

    @PostMapping("/queryDeviceDetail")
    public R<DeviceVo> queryDeviceDetail(@RequestBody DeviceDto deviceDto) {
        return R.ok(deviceService.queryDeviceDetail(deviceDto));
    }

    @PostMapping("/queryThingModelPublished")
    public AjaxResult  queryThingModelPublished(@RequestBody DeviceDto deviceDto) {
        return deviceService.queryThingModelPublished(deviceDto);
    }

    @PostMapping("/queryDevicePropertyStatus")
    public AjaxResult queryDevicePropertyStatus(@RequestBody QueryDevicePropertyStatusRequest request) {
        return deviceService.queryDevicePropertyStatus(request);
    }

    @PutMapping
    public AjaxResult edit(@RequestBody Device device)
    {
        deviceService.updateDeviceNickName(device);
        return success();
    }

    @DeleteMapping
    public AjaxResult deleteDevice(@RequestBody DeleteDeviceRequest request){
        return toAjax(deviceService.deleteDevice(request));
    }
}
