package com.zzyl.serve.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zzyl.common.constant.CacheConstants;
import com.zzyl.common.utils.DateUtils;
import com.zzyl.serve.vo.NursingLevelVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.zzyl.serve.mapper.NursingLevelMapper;
import com.zzyl.serve.domain.NursingLevel;
import com.zzyl.serve.service.INursingLevelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.Arrays;

/**
 * 护理等级Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-10-04
 */
@Service
public class NursingLevelServiceImpl extends ServiceImpl<NursingLevelMapper, NursingLevel> implements INursingLevelService
{
    @Autowired
    private NursingLevelMapper nursingLevelMapper;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    /**
     * 查询护理等级
     * 
     * @param id 护理等级主键
     * @return 护理等级
     */
    @Override
    public NursingLevel selectNursingLevelById(Long id)
    {
        return getById(id);
    }

    /**
     * 查询护理等级列表
     * 
     * @param nursingLevel 护理等级
     * @return 护理等级
     */
    @Override
    public List<NursingLevel> selectNursingLevelList(NursingLevel nursingLevel)
    {
        return nursingLevelMapper.selectNursingLevelList(nursingLevel);
    }

    /**
     * 新增护理等级
     * 
     * @param nursingLevel 护理等级
     * @return 结果
     */
    @Override
    public int insertNursingLevel(NursingLevel nursingLevel)
    {
        // 1.保存数据
        boolean result = save(nursingLevel);
        // 2.删除缓存
        deleteCache();
        // 3.返回结果
        return result ? 1 : 0;
    }

    /**
     * 修改护理等级
     * 
     * @param nursingLevel 护理等级
     * @return 结果
     */
    @Override
    public int updateNursingLevel(NursingLevel nursingLevel)
    {
        // 1.保存数据
        boolean result = updateById(nursingLevel);
        // 2.删除缓存
        deleteCache();
        // 3.返回结果
        return result ? 1 : 0;
    }

    /**
     * 批量删除护理等级
     * 
     * @param ids 需要删除的护理等级主键
     * @return 结果
     */
    @Override
    public int deleteNursingLevelByIds(Long[] ids)
    {
        // 1.删除数据
        boolean result = removeByIds(Arrays.asList(ids));
        // 2.删除缓存
        deleteCache();
        // 3.返回结果
        return result ? 1 : 0;
    }

    private void deleteCache() {
        redisTemplate.delete(CacheConstants.NURSING_LEVEL_ALL);
    }

    /**
     * 删除护理等级信息
     * 
     * @param id 护理等级主键
     * @return 结果
     */
    @Override
    public int deleteNursingLevelById(Long id)
    {
        return removeById(id) ? 1 : 0;
    }

    /**
     * 查询所有启用的护理等级
     *
     * @return 护理等级列表
     */
    @Override
    public List<NursingLevel> getAllLevels() {
        // 1.先查询Redis中的缓存数据
        redisTemplate.opsForValue().get(CacheConstants.NURSING_LEVEL_ALL);
        List<NursingLevel> nursingLevels = (List<NursingLevel>) redisTemplate.opsForValue().get(CacheConstants.NURSING_LEVEL_ALL);

        // 2.判断缓存是否为空
        if (nursingLevels != null && nursingLevels.size() > 0) {
            return nursingLevels;
        }

        // 3.缓存中没有对应的数据，先查数据，再更新缓存
        LambdaQueryWrapper<NursingLevel> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(NursingLevel::getStatus, 1);
        nursingLevels = list(queryWrapper);
        // 更新缓存
        redisTemplate.opsForValue().set(CacheConstants.NURSING_LEVEL_ALL, nursingLevels);
        return nursingLevels;
    }

    /**
     * 查询护理等级Vo列表
     *
     * @param nursingLevel 条件
     * @return 结果
     */
    @Override
    public List<NursingLevelVo> selectNursingLevelVoList(NursingLevel nursingLevel) {
        return nursingLevelMapper.selectNursingLevelVoList(nursingLevel);
    }
}
