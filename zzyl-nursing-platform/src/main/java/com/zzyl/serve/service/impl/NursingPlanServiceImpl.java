package com.zzyl.serve.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.zzyl.common.constant.CacheConstants;
import com.zzyl.common.utils.DateUtils;
import com.zzyl.common.utils.bean.BeanUtils;
import com.zzyl.serve.domain.NursingProjectPlan;
import com.zzyl.serve.dto.NursingPlanDto;
import com.zzyl.serve.mapper.NursingProjectPlanMapper;
import com.zzyl.serve.vo.NursingPlanVo;
import com.zzyl.serve.vo.NursingProjectPlanVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.zzyl.serve.mapper.NursingPlanMapper;
import com.zzyl.serve.domain.NursingPlan;
import com.zzyl.serve.service.INursingPlanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

/**
 * 护理计划Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-10-04
 */
@Service
public class NursingPlanServiceImpl extends ServiceImpl<NursingPlanMapper, NursingPlan> implements INursingPlanService
{
    @Autowired
    private NursingPlanMapper nursingPlanMapper;

    @Autowired
    private NursingProjectPlanMapper nursingProjectPlanMapper;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    /**
     * 查询护理计划
     * 
     * @param id 护理计划主键
     * @return 护理计划
     */
    @Override
    public NursingPlanVo selectNursingPlanById(Long id)
    {
        // 1.查询护理计划基本信息
        NursingPlan nursingPlan = nursingPlanMapper.selectNursingPlanById(id);

        // 2.根据护理计划ID查询护理计划关联的护理项目
        List<NursingProjectPlanVo> projectPlans = nursingProjectPlanMapper.selectByPlanId(id);

        // 3.封装数据，返回
        NursingPlanVo nursingPlanVo = new NursingPlanVo();
        BeanUtils.copyProperties(nursingPlan, nursingPlanVo);
        nursingPlanVo.setProjectPlans(projectPlans);

        // 4.返回结果
        return nursingPlanVo;
    }

    /**
     * 查询护理计划列表
     * 
     * @param nursingPlan 护理计划
     * @return 护理计划
     */
    @Override
    public List<NursingPlan> selectNursingPlanList(NursingPlan nursingPlan)
    {
        return nursingPlanMapper.selectNursingPlanList(nursingPlan);
    }

    /**
     * 新增护理计划
     * 
     * @param nursingPlanDto 护理计划
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertNursingPlan(NursingPlanDto nursingPlanDto)
    {
        // 1.保存护理计划基本信息
        // 对象属性拷贝
        NursingPlan nursingPlan = new NursingPlan();
        BeanUtils.copyProperties(nursingPlanDto, nursingPlan);
        nursingPlan.setCreateTime(DateUtils.getNowDate());
        nursingPlanMapper.insertNursingPlan(nursingPlan);

        // 2.批量保存护理计划和护理项目的关联关系
        List<NursingProjectPlan> projectPlans = nursingPlanDto.getProjectPlans();

        // 补全护理计划id
        projectPlans.forEach(nursingProjectPlan -> nursingProjectPlan.setPlanId(nursingPlan.getId()));

        // 批量保存护理计划和护理项目的关联关系
        Long result = nursingProjectPlanMapper.batchSave(projectPlans);

        // 删除缓存
        deleteCache();

        return result > 0 ? 1 : 0;
    }

    /**
     * 修改护理计划
     * 
     * @param nursingPlanDto 护理计划
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateNursingPlan(NursingPlanDto nursingPlanDto)
    {
        // 1.更新护理项目信息
        List<NursingProjectPlan> projectPlans = nursingPlanDto.getProjectPlans();

        if (projectPlans != null && !projectPlans.isEmpty()) {
            // 1.1根据护理计划id删除护理计划关联表中的护理项目
            nursingProjectPlanMapper.deleteByPlanId(nursingPlanDto.getId());
            // 1.2批量保存护理计划和护理项目的关联关系
            // 补全护理计划id
            projectPlans.forEach(nursingProjectPlan -> nursingProjectPlan.setPlanId(nursingPlanDto.getId()));
            nursingProjectPlanMapper.batchSave(projectPlans);
        }

        // 2.更新基本信息
        NursingPlan nursingPlan = new NursingPlan();
        BeanUtils.copyProperties(nursingPlanDto, nursingPlan);

//        nursingPlanMapper.updateNursingPlan(nursingPlan);
        boolean result = updateById(nursingPlan);
        // 3.删除缓存
        deleteCache();
        // 4.返回结果
        return result ? 1 : 0;
    }

    /**
     * 批量删除护理计划
     * 
     * @param ids 需要删除的护理计划主键
     * @return 结果
     */
    @Override
    public int deleteNursingPlanByIds(Long[] ids)
    {
        // 1.批量删除护理计划关联的护理项目
        nursingProjectPlanMapper.batchDeleteByPlanIds(ids);
        // 2.删除缓存
        deleteCache();
        // 2.删除护理计划
        boolean result = removeByIds(Arrays.asList(ids));
        return result ? 1 : 0;
    }

    /**
     * 删除护理计划信息
     * 
     * @param id 护理计划主键
     * @return 结果
     */
    @Override
    public int deleteNursingPlanById(Long id)
    {
        // 1.删除数据
        boolean result = removeById(id);
        // 2.删除缓存
        deleteCache();
        // 3.返回结果
        return result ? 1 : 0;
    }

    private void deleteCache() {
        redisTemplate.delete(CacheConstants.NURSING_PLAN_ALL);
    }

    /**
     * 查询所有护理计划
     *
     * @return 护理计划列表
     */
    @Override
    public List<NursingPlan> getAllNursingPlans() {
        // 1.先查询缓存，如果缓存中有直接返回
        List<NursingPlan> nursingPlans = (List<NursingPlan>) redisTemplate.opsForValue().get(CacheConstants.NURSING_PLAN_ALL);

        if (ObjectUtils.isNotEmpty(nursingPlans)) {
            return nursingPlans;
        }

        // 2.如果缓存中没有，再查询数据库，并将结果添加到缓存中
        LambdaQueryWrapper<NursingPlan> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(NursingPlan::getStatus, 1);
        nursingPlans = list(queryWrapper);
        // 将结果添加到缓存
        redisTemplate.opsForValue().set(CacheConstants.NURSING_PLAN_ALL, nursingPlans);

        return nursingPlans;
    }
}
