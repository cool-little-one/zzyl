package com.zzyl.serve.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.serve.vo.NursingProjectPlanVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import com.zzyl.serve.domain.NursingProjectPlan;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 护理计划和项目关联Mapper接口
 * 
 * @author ruoyi
 * @date 2024-10-04
 */
@Mapper
public interface NursingProjectPlanMapper extends BaseMapper<NursingProjectPlan>
{
    /**
     * 查询护理计划和项目关联
     * 
     * @param id 护理计划和项目关联主键
     * @return 护理计划和项目关联
     */
    public NursingProjectPlan selectNursingProjectPlanById(Long id);

    /**
     * 查询护理计划和项目关联列表
     * 
     * @param nursingProjectPlan 护理计划和项目关联
     * @return 护理计划和项目关联集合
     */
    public List<NursingProjectPlan> selectNursingProjectPlanList(NursingProjectPlan nursingProjectPlan);

    /**
     * 新增护理计划和项目关联
     * 
     * @param nursingProjectPlan 护理计划和项目关联
     * @return 结果
     */
    public int insertNursingProjectPlan(NursingProjectPlan nursingProjectPlan);

    /**
     * 修改护理计划和项目关联
     * 
     * @param nursingProjectPlan 护理计划和项目关联
     * @return 结果
     */
    public int updateNursingProjectPlan(NursingProjectPlan nursingProjectPlan);

    /**
     * 删除护理计划和项目关联
     * 
     * @param id 护理计划和项目关联主键
     * @return 结果
     */
    public int deleteNursingProjectPlanById(Long id);

    /**
     * 批量删除护理计划和项目关联
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteNursingProjectPlanByIds(Long[] ids);

    /**
     * 批量保存护理计划和护理项目的关联关系
     * @param projectPlans
     * @return
     */
    Long batchSave(@Param("projectPlans") List<NursingProjectPlan> projectPlans);

    /**
     * 根据计划id查询关联项目
     * @param id 计划id
     * @return  关联的护理项目列表
     */
    @Select("select * from nursing_project_plan where plan_id = #{id}")
    List<NursingProjectPlanVo> selectByPlanId(@Param("id") Long id);

    /**
     * 根据计划id删除关联
     * @param id    护理计划id
     */
    @Delete("delete from nursing_project_plan where plan_id = #{id}")
    void deleteByPlanId(@Param("id") Long id);

    /**
     * 批量删除关联
     * @param ids
     */
    void batchDeleteByPlanIds(@Param("ids") Long[] ids);
}
