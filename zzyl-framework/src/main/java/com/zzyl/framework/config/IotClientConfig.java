package com.zzyl.framework.config;

import com.aliyun.iot20180120.Client;
import com.aliyun.teaopenapi.models.Config;
import com.zzyl.framework.config.properties.AliIoTConfigProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IotClientConfig {
    /**
     * 阿里云IoT平台客户端
     * @param aliIoTConfigProperties    配置信息
     * @return  客户端对象
     * @throws Exception    异常
     */
    @Bean
    public Client client(AliIoTConfigProperties aliIoTConfigProperties) throws Exception {
        Config config = new Config();
        // 从环境变量中读取阿里云的访问凭证
        config.accessKeyId = System.getenv("OSS_ACCESS_KEY_ID");
        config.accessKeySecret = System.getenv("OSS_ACCESS_KEY_SECRET");
        // 您的可用区ID 默认上海
        config.regionId = aliIoTConfigProperties.getRegionId();

        return new Client(config);
    }
}
