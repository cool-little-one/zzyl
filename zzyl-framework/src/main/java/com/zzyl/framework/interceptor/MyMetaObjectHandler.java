package com.zzyl.framework.interceptor;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.zzyl.common.core.domain.model.LoginUser;
import com.zzyl.common.utils.SecurityUtils;
import lombok.SneakyThrows;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Component
public class MyMetaObjectHandler implements MetaObjectHandler {


    @Autowired
    private HttpServletRequest request;

    // 是否需要排除
    @SneakyThrows
    public boolean isExclude() {
        try {
            String requestURI = request.getRequestURI();
            if(requestURI.startsWith("/member")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "createTime", Date.class, new Date());
        if (!isExclude()) {
            // 自动填充创建人
            this.strictInsertFill(metaObject, "createBy", String.class, String.valueOf(getLoginUser()));
        }

    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateTime", new Date(), metaObject);
        if (!isExclude()) {
            this.setFieldValByName("updateBy", String.valueOf(getLoginUser()), metaObject);
        }
//        this.strictUpdateFill(metaObject, "updateTime", Date.class, new Date());
//        this.strictUpdateFill(metaObject, "updateBy", String.class, String.valueOf(getLoginUser()));
    }

    public Long getLoginUser() {
        try {
            LoginUser loginUser = SecurityUtils.getLoginUser();

            if (loginUser != null) {
                return loginUser.getUserId();
            }
            return 1L;
        } catch (Exception e) {
            // 如果获取当前登录人信息报错，返回默认值1
            return 1L;
        }
    }
}
