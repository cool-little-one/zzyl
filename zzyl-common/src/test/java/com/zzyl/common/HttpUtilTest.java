package com.zzyl.common;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class HttpUtilTest {
    @Test
    public void testGet() {
        String result = HttpUtil.get("https://www.baidu.com");
        System.out.println(result);
    }

    @Test
    public void testGetWithParam() {
        Map<String, Object> params = new HashMap<>();
        params.put("pageNum", 1);
        params.put("pageSize", 10);

        String result = HttpUtil.get("http://localhost:8080/serve/project/list", params);
        System.out.println(result);
    }

    @Test
    public void testGetWithParam2() {
        Map<String, Object> params = new HashMap<>();
        params.put("pageNum", 1);
        params.put("pageSize", 3);

        HttpResponse response = HttpUtil.createGet("http://localhost:8080/serve/project/list")
                .form(params)
                .header("authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6Ijg1YmIwOTYyLWZmODctNDZmYi1iZDg3LWYwNzI2MDU0MzY1NCJ9.fuLYBc26YLhjT30FFP762g09qjh-zyLxpQ0IrZ_CLMklMEVNXhhnP8dA7YuJaP_FRhuJUNfKmp9NG3z9ONehQg")
                .execute();

        String body = response.body();  // JSON格式的数据
        System.out.println(body);
    }

    @Test
    public void testPostWithParam() {
        String url = "http://localhost:8080/serve/project";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("name", "护理项目测试");
        paramMap.put("orderNo", 1);
        paramMap.put("unit", "次");
        paramMap.put("price", 10.00);
        paramMap.put("image", "https://yjy-slwl-oss.oss-cn-hangzhou.aliyuncs.com/ae7cf766-fb7b-49ff-a73c-c86c25f280e1.png");
        paramMap.put("nursingRequirement", "无特殊要求");
        paramMap.put("status", 1);

        String params = JSONUtil.toJsonStr(paramMap);

        String result = HttpUtil.post(url, params);
        System.out.println(result);
    }

    @Test
    public void testPostWithParam2() {
        String url = "http://localhost:8080/serve/project";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("name", "护理项目测试");
        paramMap.put("orderNo", 1);
        paramMap.put("unit", "次");
        paramMap.put("price", 10.00);
        paramMap.put("image", "https://yjy-slwl-oss.oss-cn-hangzhou.aliyuncs.com/ae7cf766-fb7b-49ff-a73c-c86c25f280e1.png");
        paramMap.put("nursingRequirement", "无特殊要求");
        paramMap.put("status", 1);

        String params = JSONUtil.toJsonStr(paramMap);

        HttpResponse response = HttpUtil.createPost(url)
                .body(params)
                .header("authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6Ijg1YmIwOTYyLWZmODctNDZmYi1iZDg3LWYwNzI2MDU0MzY1NCJ9.fuLYBc26YLhjT30FFP762g09qjh-zyLxpQ0IrZ_CLMklMEVNXhhnP8dA7YuJaP_FRhuJUNfKmp9NG3z9ONehQg")
                .execute();

        // 判断，如果有返回结果，再获取结果
        if (response.isOk()) {
            String body = response.body();
            System.out.println(body);
        } else {
            System.out.println("请求失败，状态码：" + response.getStatus());
        }
    }
}
