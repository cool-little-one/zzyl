package com.zzyl.common;

import com.baidubce.qianfan.Qianfan;
import com.baidubce.qianfan.core.auth.Auth;
import com.baidubce.qianfan.model.chat.ChatResponse;

public class QianfanTest {

    public static void main(String[] args) {
        // 使用安全认证AK/SK鉴权，替换下列示例中参数，安全认证Access Key替换your_iam_ak，Secret Key替换your_iam_sk
        Qianfan qianfan = new Qianfan(Auth.TYPE_OAUTH, "LBAVNXY6zjYYe9FEwjpBJkon", "gi0HE1cVY9H82ZvzSxHF6TVLijU9mZaa");

        // 指定模型
        ChatResponse resp = qianfan.chatCompletion()
                .model("ERNIE-4.0-8K-Preview")
                .addMessage("user", "你给我讲两个笑话")
                .temperature(0.9)
                .maxOutputTokens(2048)
                .responseFormat("json_object")
                .execute();
        System.out.println(resp.getResult());
    }
}
