package com.zzyl;

import com.zzyl.nursing.service.WechatService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WechatTest {

    @Autowired
    private WechatService wechatService;

    @Test
    public void testGetOpenId() {
        String openid = wechatService.getOpenid("0b10PYFa1TF9kI0SbLFa1pzMts00PYFZ");
        System.out.println(openid);
    }

    @Test
    public void testGetPhoneNo() {
        String phoneNumber = wechatService.getPhoneNumber("962fe705a63d5dc50715484473ec58bd04ce8b46ad7cfa7c9db5ac647af3d00e");
        System.out.println(phoneNumber);
    }
}
