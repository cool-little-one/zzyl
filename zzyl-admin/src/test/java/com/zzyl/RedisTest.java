package com.zzyl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@SpringBootTest
public class RedisTest {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Test
    public void test() {
        System.out.println(redisTemplate);
    }

    @Test
    public void testString() {
        redisTemplate.opsForValue().set("name", "喜羊羊");
        System.out.println(redisTemplate.opsForValue().get("name"));
        // 指定超时时间
        redisTemplate.opsForValue().set("name2", "懒羊羊", 10, TimeUnit.SECONDS);
        // setnx
        redisTemplate.opsForValue().setIfAbsent("name2", "沸羊羊");
        System.out.println(redisTemplate.opsForValue().get("name2"));   // 懒羊羊
    }

    @Test
    public void testHash() {
        // hset
        redisTemplate.opsForHash().put("student", "name", "小明");
        redisTemplate.opsForHash().put("student", "age", "18");
        // hget
        System.out.println(redisTemplate.opsForHash().get("student", "name"));  // 小明
        // hkeys
        Set<Object> students = redisTemplate.opsForHash().keys("student");
        System.out.println(students);       // [name, age]
        // hvals
        List<Object> student = redisTemplate.opsForHash().values("student");
        System.out.println(student);    // [小明, 18]
        // hdel
        redisTemplate.opsForHash().delete("student", "age");
        System.out.println(redisTemplate.opsForHash().get("student", "age"));   // null
    }

    @Test
    public void testList() {
        redisTemplate.opsForList().leftPush("list3", "1");
        redisTemplate.opsForList().leftPushAll("list3", "2", "3");
        List<String> list3 = redisTemplate.opsForList().range("list3", 0, -1);
        System.out.println(list3);      // 3 2 1
        // rpop，返回的是被删除的元素
        System.out.println(redisTemplate.opsForList().rightPop("list3"));   // 1
        System.out.println(redisTemplate.opsForList().size("list3"));   // 2
    }

    @Test
    public void testSet() {
        // sadd
        redisTemplate.opsForSet().add("seta", "1", "2", "3");
        redisTemplate.opsForSet().add("setb", "3", "4", "5");
        // smembers: 获取集合中所有元素
        Set<String> seta = redisTemplate.opsForSet().members("seta");
        System.out.println(seta);       // [1, 2, 3]
        // scard: 获取集合中元素的个数
        System.out.println(redisTemplate.opsForSet().size("seta")); // 3
        // sdiff: 求差集
        System.out.println(redisTemplate.opsForSet().difference("seta", "setb"));  // [1, 2]
        // sinter: 求交集
        System.out.println(redisTemplate.opsForSet().intersect("seta", "setb"));  // [3]
        // sunion: 求并集
        System.out.println(redisTemplate.opsForSet().union("seta", "setb"));    // [1, 2, 3, 4, 5]
    }

    @Test
    public void testZSet() {
        redisTemplate.opsForZSet().add("zseta", "xiyangyang", 100);
        redisTemplate.opsForZSet().add("zseta", "lanyangyang", 90);
        redisTemplate.opsForZSet().add("zseta", "meiyangyang", 120);
        // zrange：查看有序集合中所有元素
        Set<String> zseta = redisTemplate.opsForZSet().range("zseta", 0, -1);
        System.out.println(zseta);  // [lanyangyang, xiyangyang, meiyangyang]
        // zincrby: 增减某个元素的分数
        redisTemplate.opsForZSet().incrementScore("zseta", "lanyangyang", 100);
        // 查看分数
        System.out.println(redisTemplate.opsForZSet().score("zseta", "lanyangyang"));   // 190.0
        // zrem: 删除元素
        redisTemplate.opsForZSet().remove("zseta", "meiyangyang");
        System.out.println(redisTemplate.opsForZSet().range("zseta", 0, -1));   // [xiyangyang, lanyangyang]
        // zrangeWithScores: 查看有序集合中所有元素，并且返回分数
        Set<ZSetOperations.TypedTuple<String>> zseta1 = redisTemplate.opsForZSet().rangeWithScores("zseta", 0, -1);
        for (ZSetOperations.TypedTuple<String> stringTypedTuple : zseta1) {
            System.out.println(stringTypedTuple.getScore());
            System.out.println(stringTypedTuple.getValue());
        }
    }

    @Test
    public void testCommon() {
        // keys: 获取所有键的集合
        Set<String> keys = redisTemplate.keys("*");
        System.out.println(keys);

        // exist: 判断键是否存在
        System.out.println(redisTemplate.hasKey("zseta"));     // true

        // type: 查看键的类型
        System.out.println(redisTemplate.type("zseta"));    // ZSET

        // del: 删除键
        redisTemplate.delete("zseta");

        // exist: 判断键是否存在
        System.out.println(redisTemplate.hasKey("zseta"));     // false
    }
}
