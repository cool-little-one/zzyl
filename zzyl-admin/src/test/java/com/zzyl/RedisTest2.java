package com.zzyl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
public class RedisTest2 {

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    @Test
    public void test() {
        User user = new User("xiyangyang", 18);
        redisTemplate.opsForValue().set("user", user);
        // 取出来
        User user1 = (User) redisTemplate.opsForValue().get("user");
        System.out.println(user1);
    }
}
